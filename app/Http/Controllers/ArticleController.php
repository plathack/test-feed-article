<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ArticleController extends Controller
{

    public function index()
    {
        $listing = Article::paginate(5)->filter((request(['category', 'search'])));

        return $listing;
    }

    public function create(Request $request)
    {
        $data = $request->validate([
            'title' => ['required', 'string'],
            'category' => ['required', 'string'],
            'author' => ['required', 'string'],
            'published_at' => ['required']
        ]);

        Article::insert($data);

        return $data;
    }
}
