<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::insert([
            [
                'title' => 'JS',
                'category' => 'Programming',
                'author' => 'Forest Gump',
                'published_at' => date('y.m.d')
            ]
        ]);
    }
}
